#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <driver/dac.h>

#define LED DAC_CHANNEL_2

void app_main()
{
  dac_output_enable(LED);
  dac_output_voltage(LED, 150);

  while(true)
  {
    for(int i = 0; i < 256; i++)
    {
      dac_output_voltage(LED, i);
      vTaskDelay(10 / portTICK_PERIOD_MS);
    }

    for(int i = 255; i > 0; i--)
    {
      dac_output_voltage(LED, i);
      vTaskDelay(10 / portTICK_PERIOD_MS);
    }
  }


}